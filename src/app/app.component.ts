import {Component, OnInit} from '@angular/core';
import {CoolLocalStorage} from '@angular-cool/storage';
import {Router} from '@angular/router';
import {UserService} from './service/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  isUserLoggedIn: boolean;


  constructor(private localStorage: CoolLocalStorage, private router: Router, private userService: UserService) {
  }

  ngOnInit(): void {
    this.isUserLoggedIn = false;
    const item = this.localStorage.getItem('userName');
    if (item) {
      this.isUserLoggedIn = true;
      this.router.navigate(['dashboard']);
    }
  }

  async logout(): Promise<void> {
    const userName = this.localStorage.getItem('userName');
    await this.userService.userLogout(userName);
    this.router.navigate(['sign-in']);
  }
}
