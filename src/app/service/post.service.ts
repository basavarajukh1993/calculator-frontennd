import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Post} from '../../model/post';

@Injectable()
export class PostService {
  constructor(private httpClient: HttpClient) {
  }


  async createPost(post: Post): Promise<void> {
    await this.httpClient.post('http://localhost:8080/post', post).toPromise();
  }

  async getPost(postId: string): Promise<Post> {
    return await this.httpClient.get<Post>(`http://localhost:8080/post/${postId}`).toPromise();
  }
}
