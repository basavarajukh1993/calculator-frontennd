import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivePost} from '../../model/active-post';

@Injectable()
export class ActivePostService {
  constructor(private httpClient: HttpClient) {
  }


  async getActivePost(userName: string): Promise<ActivePost[]> {
    return await this.httpClient.get<ActivePost[]>(`http://localhost:8080/active-post?userName=${userName}&page=0&size=10`).toPromise();
  }
}
