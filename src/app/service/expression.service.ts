import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Expression} from '../../model/expression';

@Injectable()
export class ExpressionService {
  constructor(private httpClient: HttpClient) {
  }

  public async createAndEvaluateExpression(expression: Expression): Promise<Expression> {
    return this.httpClient.post<Expression>('http://localhost:8080/expression/evaluate', expression).toPromise();
  }

  async getExpression(expressionId: string): Promise<Expression> {
    return await this.httpClient.get<Expression>(`http://localhost:8080/expression/${expressionId}`).toPromise();
  }
}
