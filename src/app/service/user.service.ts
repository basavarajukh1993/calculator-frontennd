import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../../model/user';

@Injectable()
export class UserService {
  constructor(private httpClient: HttpClient) {
  }

  public async userLogin(user: User): Promise<string> {
    const requestOptions: object = {
      headers: new HttpHeaders(),
      responseType: 'text'
    };
    return await this.httpClient.post<string>('http://localhost:8080/user/login', user, requestOptions).toPromise();
  }

  public async userLogout(userName: string): Promise<string> {
    return await this.httpClient.post<string>(`http://localhost:8080/user/${userName}/login`, '').toPromise();
  }
}
