import {Component, OnInit} from '@angular/core';
import {ExpressionService} from '../service/expression.service';
import {Expression, Operator} from '../../model/expression';
import {PostService} from '../service/post.service';
import {CoolLocalStorage} from '@angular-cool/storage';
import {Post, PostType} from '../../model/post';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  providers: [ExpressionService, PostService]
})
export class CalculatorComponent implements OnInit {
  operator = Operator;
  result: string;
  resultEntityId: string;

  constructor(private service: ExpressionService, private postService: PostService, private localStorage: CoolLocalStorage,
              private toasterService: ToastrService, private router: Router) {
  }

  ngOnInit(): void {
    const item = this.localStorage.getItem('userName');
    if (!item) {
      this.router.navigate(['sign-in']);
    }
  }


  public async evaluateExpression(calculatorForm: any): Promise<void> {
    const operand1 = calculatorForm.controls.operand1.value;
    const operand2 = calculatorForm.controls.operand2.value;
    const operator = calculatorForm.controls.operator.value;

    if (operand1 && operand2 && operator) {
      const expression = new Expression(operand1, operand2, operator);
      const response: Expression = await this.service.createAndEvaluateExpression(expression);
      this.result = response.result;
      this.resultEntityId = response.id;
    }
  }

  async postExpresion(): Promise<void> {
    const userName = this.localStorage.getItem('userName');
    if (userName && this.resultEntityId) {
      const post = new Post(userName, PostType.EXPRESSION, this.resultEntityId);
      await this.postService.createPost(post);
      this.toasterService.success('Successfully created the post');
    }
  }

  resetRest(): void {
    if (this.result && this.resultEntityId) {
      this.result = undefined;
      this.resultEntityId = undefined;
    }
  }
}
