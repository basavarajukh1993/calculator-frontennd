import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SignInComponent} from './sign-in/sign-in.component';
import {CalculatorComponent} from './calculator/calculator.component';
import {ActivePostComponent} from './active-post/active-post.component';
import {DashboardComponent} from './dashboard/dashboard.component';

const routes: Routes = [
  {path: 'sign-in', component: SignInComponent},
  {path: 'calculator', component: CalculatorComponent},
  {path: 'post', component: ActivePostComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: '', redirectTo: 'sign-in', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
