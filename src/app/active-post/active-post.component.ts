import {Component, OnInit} from '@angular/core';
import {ActivePostService} from '../service/active-post.service';
import {CoolLocalStorage} from '@angular-cool/storage';
import {ExpressionService} from '../service/expression.service';
import {PostService} from '../service/post.service';
import {PostType} from '../../model/post';

@Component({
  selector: 'app-active-post',
  templateUrl: './active-post.component.html',
  styleUrls: ['./active-post.component.css'],
  providers: [ActivePostService, PostService, ExpressionService]
})
export class ActivePostComponent implements OnInit {
  posts = [];

  constructor(private activePostService: ActivePostService, private postService: PostService, private expressionService: ExpressionService,
              private localStorage: CoolLocalStorage) {
  }

  async ngOnInit(): Promise<void> {
    const userName = this.localStorage.getItem('userName');
    const activePosts = await this.activePostService.getActivePost(userName);
    for (const activePost of activePosts) {
      const post = await this.postService.getPost(activePost.postId);
      if (post.postType === PostType.EXPRESSION) {
        const expression = await this.expressionService.getExpression(post.postEntityId);
        const postInfo = {
          expression,
          postedBy: post.postedBy
        };
        this.posts.push(postInfo);
      }
    }
  }
}
