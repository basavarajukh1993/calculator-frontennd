import {Component, OnInit} from '@angular/core';
import {CoolLocalStorage} from '@angular-cool/storage';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
  constructor(private localStorage: CoolLocalStorage, private router: Router) {
  }

  ngOnInit(): void {
    const item = this.localStorage.getItem('userName');
    if (!item) {
      this.router.navigate(['sign-in']);
    }
  }
}
