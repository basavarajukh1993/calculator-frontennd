import {Component, OnInit} from '@angular/core';
import {CoolLocalStorage} from '@angular-cool/storage';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../service/user.service';
import {User} from '../../model/user';

@Component({
  selector: 'app-sign-in',
  templateUrl: 'sign-in.component.html',
  styleUrls: ['sign-in.component.css']
})
export class SignInComponent implements OnInit {
  constructor(private localStorage: CoolLocalStorage, private route: ActivatedRoute, private router: Router,
              private userService: UserService) {
  }

  public async forwardToCalculator(signInForm: any): Promise<void> {
    const value = signInForm.controls.userName.value;
    if (value) {
      const user = new User(value);
      const sessionId = await this.userService.userLogin(user);
      this.localStorage.setItem('userName', value);
      this.localStorage.setItem('sessionId', sessionId);
      await this.router.navigate(['/calculator']);
    }
  }

  ngOnInit(): void {
    const item = this.localStorage.getItem('userName');
    if (!item) {
      this.router.navigate(['sign-in']);
    }
  }
}
