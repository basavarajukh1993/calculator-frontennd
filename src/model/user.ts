export class User {
  public readonly userName: string;

  constructor(userName: string) {
    this.userName = userName;
  }
}
