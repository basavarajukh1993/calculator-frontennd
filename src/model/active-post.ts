export class ActivePost {
  public readonly id: string;
  public readonly postId: string;
  public readonly activeUserName: string;

  constructor(id: string, postId: string, activeUserName: string) {
    this.id = id;
    this.postId = postId;
    this.activeUserName = activeUserName;
  }
}
