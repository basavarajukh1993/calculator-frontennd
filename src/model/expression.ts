export class Expression {
  public id: string;
  public readonly result: string;
  public readonly operand1: string;
  public readonly operand2: string;
  public readonly operator: Operator;

  constructor(operand1: string, operand2: string, operator: Operator, result?: string) {
    this.result = result;
    this.operand1 = operand1;
    this.operand2 = operand2;
    this.operator = operator;
  }
}

export enum Operator {
  ADD = ('ADD'), SUBTRACT = ('SUBTRACT'), DIVIDE = ('DIVIDE'), MULTIPLY = ('MULTIPLY')
}
