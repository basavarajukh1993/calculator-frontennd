export class Post {
  public readonly id: string;
  public readonly postedBy: string;
  public readonly postType: PostType;
  public readonly postEntityId: string;

  constructor(postedBy: string, postType: PostType, postEntityId: string) {
    this.postedBy = postedBy;
    this.postType = postType;
    this.postEntityId = postEntityId;
  }
}

export enum PostType {
  EXPRESSION = ('EXPRESSION')
}
