import {suite} from 'mocha-typescript';
import {instance, mock, verify, when} from 'ts-mockito';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs';
import {ActivePostService} from '../../src/app/service/active-post.service';
import {ActivePost} from '../../src/model/active-post';

@suite
export class ActivePostServiceSpec {
  private httpClient = mock(HttpClient);
  private service: ActivePostService;

  public before(): void {
    this.service = new ActivePostService(instance(this.httpClient));
  }

  @test
  public async shouldGetActivePostForUser(): Promise<void> {
    const activePost1 = new ActivePost('activePostId', 'postId', 'userName');
    const activePost2 = new ActivePost('activePostId', 'postId', 'userName');
    const activePost3 = new ActivePost('activePostId', 'postId', 'userName');
    const activePosts = [activePost1, activePost2, activePost3];
    when(this.httpClient.get('http://localhost:8080/active-post?userName=userName&page=0&size=10')).thenReturn(of(activePosts));

    await this.service.getActivePost('userName');

    verify(this.httpClient.get('http://localhost:8080/active-post?userName=userName&page=0&size=10')).once();
  }
}
