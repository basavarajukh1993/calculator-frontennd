import {suite} from 'mocha-typescript';
import {PostService} from '../../src/app/service/post.service';
import {instance, mock, verify, when} from 'ts-mockito';
import {HttpClient} from '@angular/common/http';
import {Post, PostType} from '../../src/model/post';
import {of} from 'rxjs';

@suite
export class PostServiceSpec {
  private httpClient = mock(HttpClient);
  private service: PostService;

  public before(): void {
    this.service = new PostService(instance(this.httpClient));
  }

  @test
  public async shouldCreatePost(): Promise<void> {
    const post = new Post('userName', PostType.EXPRESSION, 'entityId');
    when(this.httpClient.post('http://localhost:8080/post', post)).thenReturn(of());

    await this.service.createPost(post);

    verify(this.httpClient.post('http://localhost:8080/post', post)).once();
  }

  @test
  public async shouldGetPostById(): Promise<void> {
    const post = new Post('postedBy', PostType.EXPRESSION, 'postId');
    when(this.httpClient.get('http://localhost:8080/post/postId')).thenReturn(of(post));

    await this.service.getPost('postId');

    verify(this.httpClient.get('http://localhost:8080/post/postId')).once();
  }
}
