import {suite} from 'mocha-typescript';
import {ExpressionService} from '../../src/app/service/expression.service';
import {HttpClient} from '@angular/common/http';
import {instance, mock, when} from 'ts-mockito';
import {Expression, Operator} from '../../src/model/expression';
import {expect} from 'chai';
import {of} from 'rxjs';

@suite
export class ExpressionServiceSpec {
  private httpClient = mock(HttpClient);
  private service: ExpressionService;

  public before(): void {
    this.service = new ExpressionService(instance(this.httpClient));
  }

  @test
  public async shouldCreateExpressionAndEvaluateExpression(): Promise<void> {
    const expression = new Expression('1', '2', Operator.ADD);
    when(this.httpClient.post('http://localhost:8080/expression/evaluate', expression)).thenReturn(of(expression));

    const actualExpression = await this.service.createAndEvaluateExpression(expression);

    expect(actualExpression).to.be.equal(expression);
  }

  @test
  public async shouldGetExpressionById(): Promise<void> {
    const expressionId = 'expressionId';
    const expression = new Expression('1', '2', Operator.ADD, '3');
    expression.id = expressionId;
    when(this.httpClient.get(`http://localhost:8080/expression/${expressionId}`)).thenReturn(of(expression));

    const actualExpression = await this.service.getExpression(expressionId);

    expect(actualExpression).to.be.equal(expression);
  }
}
