import {suite} from 'mocha-typescript';
import {deepEqual, instance, mock, verify, when} from 'ts-mockito';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserService} from '../../src/app/service/user.service';
import {User} from '../../src/model/user';
import {expect} from 'chai';
import {of} from 'rxjs';

@suite
export class UserServiceSpec {
  private http = mock(HttpClient);
  private userService: UserService;

  public before(): void {
    this.userService = new UserService(instance(this.http));
  }

  @test
  public async shouldAllowUserToLogin(): Promise<void> {
    const user = new User('userName');
    const requestOptions: object = {
      headers: new HttpHeaders(),
      responseType: 'text'
    };

    when(this.http.post('http://localhost:8080/user/login', user, deepEqual(requestOptions))).thenReturn(of('sessionId'));

    const sessionId = await this.userService.userLogin(user);

    expect(sessionId).to.be.deep.equal('sessionId');
  }

  @test
  public async shouldAllowUserToLogout(): Promise<void> {
    const userName = 'userName';
    when(this.http.post(`http://localhost:8080/user/${userName}/login`, '')).thenReturn(of());

    await this.userService.userLogout(userName);

    verify(this.http.post(`http://localhost:8080/user/${userName}/login`, '')).once();
  }

}
