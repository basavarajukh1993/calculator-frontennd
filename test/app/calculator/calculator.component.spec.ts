import {suite} from 'mocha-typescript';
import {CalculatorComponent} from '../../../src/app/calculator/calculator.component';
import {ExpressionService} from '../../../src/app/service/expression.service';
import {deepEqual, instance, mock, verify, when} from 'ts-mockito';
import {expect} from 'chai';
import {Expression, Operator} from '../../../src/model/expression';
import {PostService} from '../../../src/app/service/post.service';
import {Post, PostType} from '../../../src/model/post';
import {CoolLocalStorage} from '@angular-cool/storage';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@suite
export class CalculatorComponentSpec {
  private component: CalculatorComponent;
  private postService = mock(PostService);
  private service = mock(ExpressionService);
  private localStorage = mock(CoolLocalStorage);
  private toasterService = mock(ToastrService);
  private router = mock(Router);

  public before(): void {
    this.component = new CalculatorComponent(instance(this.service), instance(this.postService), instance(this.localStorage),
      instance(this.toasterService), instance(this.router));
  }

  @test
  public shouldRedirectToSignInPageWhenNoUserIsLoggedIn(): void {
    when(this.localStorage.getItem('userName')).thenReturn(undefined);

    this.component.ngOnInit();

    verify(this.router.navigate(deepEqual(['sign-in']))).once();
  }

  @test
  public shouldNotRedirectWhenUserIsAlreadyLoggedIn(): void {
    when(this.localStorage.getItem('userName')).thenReturn('userName');

    this.component.ngOnInit();

    verify(this.router.navigate(deepEqual(['sign-in']))).never();
  }


  @test
  public async shouldEvaluateExpression(): Promise<void> {
    const expression = new Expression('1', '2', Operator.ADD);
    const evaluatedExpression = new Expression('1', '2', Operator.ADD, '3');
    evaluatedExpression.id = 'evaluatedExpressionId';

    const form = {
      controls: {
        operand1: {value: '1'},
        operand2: {value: '2'},
        operator: {value: Operator.ADD}
      }
    };
    when(this.service.createAndEvaluateExpression(deepEqual(expression))).thenResolve(evaluatedExpression);

    await this.component.evaluateExpression(form);

    verify(this.service.createAndEvaluateExpression(deepEqual(expression))).once();
    expect(this.component.result).to.equal('3');
    expect(this.component.resultEntityId).to.equal('evaluatedExpressionId');
  }

  @test
  public async shouldAbleToCreatePostAfterEvaluation(): Promise<void> {
    this.component.resultEntityId = 'entityId';
    when(this.localStorage.getItem('userName')).thenReturn('userName');
    const post = new Post('userName', PostType.EXPRESSION, 'entityId');
    when(this.postService.createPost(post)).thenResolve();

    await this.component.postExpresion();

    verify(this.postService.createPost(deepEqual(post))).once();
    verify(this.toasterService.success('Successfully created the post')).once();
  }

  @test
  public shouldClearGlobalResultForAnyChangesSecondTimeOnwards(): void {
    this.component.result = '3';
    this.component.resultEntityId = 'entityId';

    this.component.resetRest();

    expect(this.component.result).to.equal(undefined);
    expect(this.component.resultEntityId).to.equal(undefined);
  }
}
