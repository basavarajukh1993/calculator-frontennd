import {suite} from 'mocha-typescript';
import {SignInComponent} from '../../../src/app/sign-in/sign-in.component';
import {CoolLocalStorage} from '@angular-cool/storage';
import {deepEqual, instance, mock, verify, when} from 'ts-mockito';
import {Router} from '@angular/router';
import {UserService} from '../../../src/app/service/user.service';
import {User} from '../../../src/model/user';

@suite
export class SignInComponentSpec {
  private component;
  private router: Router = mock(Router);
  private userService = mock(UserService);
  private storage: CoolLocalStorage = mock(CoolLocalStorage);

  public before(): void {
    this.component = new SignInComponent(instance(this.storage), null, instance(this.router), instance(this.userService));
  }

  @test
  public async shouldStepInAndSaveUserNameToLocalStorage(): Promise<void> {
    const user = new User('userName');
    when(this.userService.userLogin(deepEqual(user))).thenResolve('sessionId');
    when(this.storage.setItem('userName', 'userName')).thenResolve();
    when(this.storage.setItem('sessionId', 'sessionId')).thenResolve();

    await this.component.forwardToCalculator({controls: {userName: {value: 'userName'}}});

    verify(this.router.navigate(deepEqual(['/calculator']))).once();
    verify(this.storage.setItem('userName', 'userName')).once();
    verify(this.storage.setItem('sessionId', 'sessionId')).once();
  }

  @test
  public shouldRedirectToSignInPageWhenNoUserIsLoggedIn(): void {
    when(this.storage.getItem('userName')).thenReturn(undefined);

    this.component.ngOnInit();

    verify(this.router.navigate(deepEqual(['sign-in']))).once();
  }

  @test
  public shouldNotRedirectWhenUserIsAlreadyLoggedIn(): void {
    when(this.storage.getItem('userName')).thenReturn('userName');

    this.component.ngOnInit();

    verify(this.router.navigate(deepEqual(['sign-in']))).never();
  }

}
