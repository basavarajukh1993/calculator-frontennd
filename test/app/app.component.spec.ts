import {suite} from 'mocha-typescript';
import {deepEqual, instance, mock, verify, when} from 'ts-mockito';
import {CoolLocalStorage} from '@angular-cool/storage';
import {Router} from '@angular/router';
import {expect} from 'chai';
import {AppComponent} from '../../src/app/app.component';
import {UserService} from '../../src/app/service/user.service';

@suite
export class AppComponentSpec {
  private router = mock(Router);
  private userService = mock(UserService);
  private localStorage = mock(CoolLocalStorage);
  private component: AppComponent;

  public before(): void {
    this.component = new AppComponent(instance(this.localStorage), instance(this.router), instance(this.userService));
  }

  @test
  public shouldNotNavigateToDashboardRouteWhenUserIsNotLogIn(): void {
    when(this.localStorage.getItem('userName')).thenReturn(undefined);

    this.component.ngOnInit();

    expect(this.component.isUserLoggedIn).to.false;
    verify(this.router.navigate(deepEqual(['dashboard']))).never();
  }

  @test
  public shouldNotNavigateToSignInRouteWhenUserLogIn(): void {
    when(this.localStorage.getItem('userName')).thenReturn('userName');

    this.component.ngOnInit();

    expect(this.component.isUserLoggedIn).to.true;
    verify(this.router.navigate(deepEqual(['dashboard']))).once();
  }

  @test
  public async shouldAbleToLogoutUser(): Promise<void> {
    when(this.localStorage.getItem('userName')).thenReturn('userName');
    when(this.userService.userLogout('userName')).thenResolve();

    await this.component.logout();

    verify(this.userService.userLogout('userName')).once();
    verify(this.router.navigate(deepEqual(['sign-in']))).once();
  }
}
