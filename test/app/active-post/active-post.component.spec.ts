import {suite} from 'mocha-typescript';
import {ActivePostService} from '../../../src/app/service/active-post.service';
import {instance, mock, when} from 'ts-mockito';
import {PostService} from '../../../src/app/service/post.service';
import {ExpressionService} from '../../../src/app/service/expression.service';
import {ActivePostComponent} from '../../../src/app/active-post/active-post.component';
import {CoolLocalStorage} from '@angular-cool/storage';
import {ActivePost} from '../../../src/model/active-post';
import {Post, PostType} from '../../../src/model/post';
import {expect} from 'chai';
import {Expression, Operator} from '../../../src/model/expression';

@suite
export class ActivePostComponentSpec {
  private postService = mock(PostService);
  private localStorage = mock(CoolLocalStorage);
  private activePostService = mock(ActivePostService);
  private expressionService = mock(ExpressionService);

  private component: ActivePostComponent;

  public before(): void {
    this.component = new ActivePostComponent(instance(this.activePostService), instance(this.postService), instance(this.expressionService),
      instance(this.localStorage));
  }

  @test
  public async shouldGetPostRelatedToLogedInUser(): Promise<void> {
    const userName = 'userName';
    const activePost1 = new ActivePost('activePostId1', 'postId1', userName);
    const activePost2 = new ActivePost('activePostId2', 'postId2', userName);
    const activePost3 = new ActivePost('activePostId3', 'postId3', userName);
    const activePosts = [activePost1, activePost2, activePost3];

    const post1 = new Post('someUser', PostType.EXPRESSION, 'expressionId1');
    const post2 = new Post('otherUser', PostType.EXPRESSION, 'expressionId2');
    const post3 = new Post('anotherUser', PostType.EXPRESSION, 'expressionId3');

    const expression1 = new Expression('1', '2', Operator.ADD, '3');
    const expression2 = new Expression('2', '1', Operator.SUBTRACT, '1');
    const expression3 = new Expression('1', '2', Operator.MULTIPLY, '2');
    const expectedPosts = [
      {
        expression: expression1,
        postedBy: 'someUser'
      },
      {
        expression: expression2,
        postedBy: 'otherUser'
      },
      {
        expression: expression3,
        postedBy: 'anotherUser'
      }
    ];

    when(this.localStorage.getItem('userName')).thenReturn(userName);
    when(this.activePostService.getActivePost(userName)).thenResolve(activePosts);
    when(this.postService.getPost('postId1')).thenResolve(post1);
    when(this.postService.getPost('postId2')).thenResolve(post2);
    when(this.postService.getPost('postId3')).thenResolve(post3);
    when(this.expressionService.getExpression('expressionId1')).thenResolve(expression1);
    when(this.expressionService.getExpression('expressionId2')).thenResolve(expression2);
    when(this.expressionService.getExpression('expressionId3')).thenResolve(expression3);

    await this.component.ngOnInit();

    expect(this.component.posts).to.deep.equal(expectedPosts);

  }
}
