import {suite} from 'mocha-typescript';
import {deepEqual, instance, mock, verify, when} from 'ts-mockito';
import {DashboardComponent} from '../../../src/app/dashboard/dashboard.component';
import {Router} from '@angular/router';
import {CoolLocalStorage} from '@angular-cool/storage';

@suite
export class DashboardComponentSpec {
  private localStorage = mock(CoolLocalStorage);
  private router = mock(Router);
  private component: DashboardComponent;

  public before(): void {
    this.component = new DashboardComponent(instance(this.localStorage), instance(this.router));
  }

  @test
  public shouldNavigateToSignInRouteWhenUserNotLogIn(): void {
    when(this.localStorage.getItem('userName')).thenReturn(undefined);

    this.component.ngOnInit();

    verify(this.router.navigate(deepEqual(['sign-in']))).once();
  }

  @test
  public shouldNotNavigateToSignInRouteWhenUserLogIn(): void {
    when(this.localStorage.getItem('userName')).thenReturn('userName');

    this.component.ngOnInit();

    verify(this.router.navigate(deepEqual(['sign-in']))).never();
  }

}
